# Springboot/Angular shop-back-products
The purpose of the application was to develop a backend to manage    products defined in a json.file into a SQL database. The backend has to handle the following REST APIs : POST, GET, PATCH and DELETE.
## Getting started

### Made with
-   Java 21.0.1
-   Maven 3.8.5
- Mysql 8.0
-   Angular
- Node.js
- Eclipse

`git clone git@gitlab.com:guillaumeseateun/shop-back-products.git` to get the project

### Spring boot
The version of Springboot is the 3.2.1.

Run ***`mvn spring-boot:run`***  to build the backend.

A tomcat server will start at the following url : http://localhost:8080/

#### Rest APIs


|Method   | URL   | Description   |
|--|--|--|
| *POST* | *api/products* | Create a new product |
| *GET* | *api/products* | Retrieve all products  |
| *GET* | *api/products/{id}* | Retrieve details for a product |
| *PATCH* | *api/products/{id}* | Update partially a product if it exists |
| *DELETE* | *api/products/{id}* | Remove a product |

### Angular

This project was generated with Angular CLI version 14.2.9.


  
Run ***`ng install`*** to build the project. . 

Run ***`ng serve --port 3000`*** for a dev server. Navigate to http://localhost:3000/. The app will automatically reload if you change any of the source files.
