package com.guillaumeseateun.altenshopbackproduct.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.guillaumeseateun.altenshopbackproduct.dto.ProductDto;
import com.guillaumeseateun.altenshopbackproduct.model.Product;
import com.guillaumeseateun.altenshopbackproduct.repository.ProductRepository;
import com.guillaumeseateun.altenshopbackproduct.service.util.Helper;

@Service
public class ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	// Retrieve all products
	public List<Product> getAll(){
		return (List<Product>) productRepository.findAll();
	}
	
	// Retrieve details for 1 product
	public Product getProduct(Integer id){
		return productRepository.findById(id)
				.orElseThrow(() -> new ProductNotFoundException("Product by id " + id + " was not found :("));
	}
	
	// Create a new product
	public Product addProduct(ProductDto productDto) {
		Product product = new Product();
		
		product.setCategory(productDto.getCategory());
		product.setCode(productDto.getCode());
		product.setDescription(productDto.getDescription());
		product.setImage(productDto.getImage());
		product.setInventoryStatus(productDto.getInventoryStatus());
		product.setName(productDto.getName());
		product.setPrice(productDto.getPrice());
		product.setQuantity(productDto.getQuantity());
		product.setRating(productDto.getRating());
		
		return productRepository.save(product);
	}
	
	// Remove 1 product
	public void deleteProduct(Integer id) {
		productRepository.deleteById(id);
	}
	
	//Update details of 1 product if it exists
	//Partially update a product if it is found
	public Product updatePartially(Integer id, Product partialUpdate) 
    {
		
		Product existingProduct = productRepository.findById(id)
				.orElseThrow(() -> new ProductNotFoundException("Product by id " + id + " was not found :("));;

		BeanUtils.copyProperties(partialUpdate, existingProduct, Helper.getNullPropertyNames(partialUpdate));
		return productRepository.save(existingProduct);
    }

}
