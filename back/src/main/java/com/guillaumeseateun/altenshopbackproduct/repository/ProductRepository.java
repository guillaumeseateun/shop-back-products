package com.guillaumeseateun.altenshopbackproduct.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.guillaumeseateun.altenshopbackproduct.model.Product;

@Repository
public interface ProductRepository extends CrudRepository<Product, Integer> {

}
