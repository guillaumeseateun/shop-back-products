package com.guillaumeseateun.altenshopbackproduct.controller;


import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.guillaumeseateun.altenshopbackproduct.dto.ProductDto;
import com.guillaumeseateun.altenshopbackproduct.model.Product;
import com.guillaumeseateun.altenshopbackproduct.service.ProductNotFoundException;
import com.guillaumeseateun.altenshopbackproduct.service.ProductService;

@RequestMapping
@RestController
public class ProductController {
	
	@Autowired
	private ProductService productService;
	
	@Autowired
	private ModelMapper modelMapper;
	
	@GetMapping("/products")
    public ResponseEntity<List<ProductDto>> viewAllProducts() 
    {
        return ResponseEntity.ok(productService.getAll().stream()
        		.map(products ->modelMapper.map(products, ProductDto.class))
        		.collect(Collectors.toCollection(ArrayList::new))
		); 
    }
	
	@PostMapping("/products")
    public ResponseEntity<ProductDto> createProduct(@RequestBody ProductDto product) 
    {
        return ResponseEntity.ok(modelMapper.map(productService.addProduct(product), ProductDto.class));
    }
	
	@GetMapping("/products/{id}")
    public ResponseEntity<ProductDto> viewProduct(@PathVariable("id") Integer id) throws ProductNotFoundException 
    {
		return ResponseEntity.ok(modelMapper.map(productService.getProduct(id), ProductDto.class));
    }
	
	@DeleteMapping("/products/{id}")
    public ResponseEntity<?> deleteProduct(@PathVariable("id") Integer id) 
    {
		productService.deleteProduct(id);
        return ResponseEntity.noContent().build();
    }
	
	@PatchMapping("/products/{id}") 
	public ResponseEntity<ProductDto> patchProduct(@PathVariable Integer id, @RequestBody Product partiallyUpdatedProduct) throws ProductNotFoundException  {
        return ResponseEntity.ok(modelMapper.map(productService.updatePartially(id, partiallyUpdatedProduct), ProductDto.class));
	}

}
